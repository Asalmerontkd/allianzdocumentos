/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.devf.demo2.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.devf.demo2.Utils;


/**
 * Created by antonio on 2/03/17.
 */


public class Marco extends View {

    public Marco(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int regx = canvas.getWidth() / 10; //rejilla de 10
        int ancho = regx * 8;

        int regy = regx; // rejilla alto
        int alto = regy * 5;


        int espacioy = ((canvas.getHeight() - alto) / 2) - regy;
        int espaciox = (canvas.getWidth() - ancho) / 2;

        int barrax = ancho / 3;
        int barray = alto / 3;

        Utils.x = barrax;
        Utils.y = barray;
        Utils.alto = alto;
        Utils.ancho = ancho;
        Utils.rejilla = 10;
        Utils.constanteX = 8;
        Utils.constanteY = 5;


        Paint myGrayPaint = new Paint();
        myGrayPaint.setColor(Color.parseColor("#000000"));
        myGrayPaint.setAlpha(0);

        canvas.drawRect(0, 0, canvas.getWidth(), espacioy, myGrayPaint);//top
        canvas.drawRect(0, espacioy, espaciox, canvas.getHeight(), myGrayPaint);//left
        canvas.drawRect(espaciox, alto + espacioy, canvas.getWidth(), canvas.getHeight(),
                myGrayPaint); //bottom
        canvas.drawRect(ancho + espaciox, espacioy, canvas.getWidth(), alto + espacioy,
                myGrayPaint); //right


        Paint myPaint = new Paint();

        myPaint.setColor(Color.parseColor("#58E2C2"));
        myPaint.setStrokeWidth(15);

        canvas.drawPoint(espaciox, espacioy, myPaint); //punto superior izquierdo
        canvas.drawPoint(ancho + espaciox, espacioy, myPaint); //punto superior derecho
        canvas.drawPoint(espaciox, alto + espacioy, myPaint); //punto inferior izquierdo
        canvas.drawPoint(ancho + espaciox, espacioy + alto, myPaint); //punto inferior derecho


        canvas.drawLine(espaciox, espacioy, espaciox + (barrax / 2), espacioy, myPaint);
        canvas.drawLine((ancho + espaciox) - (barrax / 2), espacioy, ancho + espaciox, espacioy,
                myPaint);

        canvas.drawLine(espaciox, alto + espacioy, espaciox + (barrax / 2), espacioy + alto,
                myPaint);
        canvas.drawLine((ancho + espaciox) - (barrax / 2), alto + espacioy, ancho + espaciox,
                espacioy + alto, myPaint);

        canvas.drawLine(espaciox, espacioy, espaciox, espacioy + (barrax / 2), myPaint);
        canvas.drawLine(espaciox, (alto + espacioy) - (barrax / 2), espaciox, alto + espacioy,
                myPaint);

        canvas.drawLine(ancho + espaciox, espacioy, ancho + espaciox, espacioy + (barrax / 2),
                myPaint);
        canvas.drawLine(ancho + espaciox, (alto + espacioy) - (barrax / 2), ancho + espaciox,
                espacioy + alto, myPaint);

    }
}
