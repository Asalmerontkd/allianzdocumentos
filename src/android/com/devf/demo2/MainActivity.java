package com.devf.demo2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;


import java.io.ByteArrayOutputStream;

import com.devf.demo2.ui.Cards;
import com.devf.demo2.Utils;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;

public class MainActivity extends CordovaPlugin {
    public CallbackContext callbackContext;
    boolean cardFlag = false;
    JSONObject cardObj = null;
    private static final int MY_SCAN_REQUEST_CODE = 37;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
      
      // TODO Auto-generated method stub
      this.callbackContext = callbackContext;
      try {
            if ("cardFunction".equals(action)) {
                Intent ca = new Intent(this.cordova.getActivity().getApplicationContext(), Cards.class);
                cordova.getActivity().startActivity(ca);
                PluginResult resultsImg = new PluginResult(PluginResult.Status.NO_RESULT);
                resultsImg.setKeepCallback(true);
                callbackContext.sendPluginResult(resultsImg);
                return true;
            }
            return false;
      } catch (Exception e) {
          e.printStackTrace();
          PluginResult res = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
          callbackContext.sendPluginResult(res);
          return false;
      }
      
  }

  public String base64Transform(Bitmap bitmap) {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
      byte[] byteArray = byteArrayOutputStream.toByteArray();
      String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
      return encoded;
  }


    @Override
  public void onResume(boolean multitasking) {
      super.onResume(multitasking);
        if (Utils.flagCard)
        {
            Utils.flagCard = false;
            PluginResult resultsImg = new PluginResult(PluginResult.Status.OK, Utils.cardInfo);
            resultsImg.setKeepCallback(false);
            callbackContext.sendPluginResult(resultsImg);
        }
        else
        {
          PluginResult resultsImg = new PluginResult(PluginResult.Status.ERROR, "Evento cancelado.");
          callbackContext.sendPluginResult(resultsImg);
        }
    }

}
