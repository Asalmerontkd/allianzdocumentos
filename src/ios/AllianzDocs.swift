@objc(AllianzDocs) class AllianzDocs : CDVPlugin {
  @objc(docFunctionLg:)
  func docFunctionLg(command: CDVInvokedUrlCommand) {
    DispatchQueue.global(qos: .background).async {
        var pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR
        )

        let msg = command.arguments[0] as? String ?? ""

        if msg.characters.count > 0 {
            let toastController: iScannerLG =
                iScannerLG()
                toastController.setPluginDelegate(forPluggin: self, andCommand: command)

            DispatchQueue.main.async {
                if "\(command.arguments[0])" == "switchCam" {
                    print("debe cambiar de camara")
                    toastController.enableCamSwitcher()
                }
                self.viewController?.present(
                    toastController,
                    animated: true,
                    completion: nil
                )
            }

            //DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            //  toastController.dismiss(
            //      animated: true,
            //      completion: nil
            //  )
            //}
        }


    }

  }

  @objc(docFunctionSm:)
  func docFunctionSm(command: CDVInvokedUrlCommand) {
    DispatchQueue.global(qos: .background).async {
        var pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR
        )

        let msg = command.arguments[0] as? String ?? ""

        if "\(command.arguments[0])" == "switchCam" {
            print("debe cambiar de camara")
        }

        if msg.characters.count > 0 {
            let toastController: iScanner =
                iScanner()
                toastController.setPluginDelegate(forPluggin: self, andCommand: command)


            DispatchQueue.main.async {
                if "\(command.arguments[0])" == "switchCam" {
                    print("debe cambiar de camara")
                    toastController.enableCamSwitcher()
                }
                self.viewController?.present(
                    toastController,
                    animated: true,
                    completion: nil
                )
            }


            //DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            //  toastController.dismiss(
            //      animated: true,
            //      completion: nil
            //  )
            //}
        }


    }


  }

  @objc(cardFunction:)
  func cardFunction(command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    let msg = command.arguments[0] as? String ?? ""

    if msg.characters.count > 0 {
      let toastController: cardScanner =
        cardScanner()
        toastController.setPluginDelegate(forPluggin: self, andCommand: command)

      self.viewController?.present(
        toastController,
        animated: true,
        completion: nil
      )

      //DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
      //  toastController.dismiss(
      //      animated: true,
      //      completion: nil
      //  )
      //}
    }


  }


}
