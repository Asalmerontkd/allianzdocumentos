//
//  viewFinderView.swift
//  ABlink_iOS
//
//  Created by Rodolfo Castillo on 3/1/17.
//  Copyright © 2017 Mariachis. All rights reserved.
//

import UIKit


class ViewFinderView: UIImageView{
    
    var ULTS = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 8))
    var ULBS = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 46))
    var URTS = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 8))
    var URBS = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 46))
    var LLTS = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 46))
    var LLBS = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 8))
    var LRTS = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 46))
    var LRBS = UIView(frame: CGRect(x: 0, y: 0, width: 46, height: 8))
    
    var TBar: UIView!
    var BBar: UIView!
    var LBar: UIView!
    var RBar: UIView!
    
    var gr1 = UIView()
    var gr2 = UIView()
    var gr3 = UIView()
    var gr4 = UIView()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        ULTS.frame.origin = CGPoint(x: 0, y: 0)
        ULBS.frame.origin = CGPoint(x: 0, y: 0)
        URTS.frame.origin = CGPoint(x: frame.width - 46, y: 0)
        URBS.frame.origin = CGPoint(x: frame.width - 8, y: 0)
        LLTS.frame.origin = CGPoint(x: 0, y: frame.height - 46)
        LLBS.frame.origin = CGPoint(x: 0, y: frame.height - 8)
        LRTS.frame.origin = CGPoint(x: frame.width - 8, y: frame.height - 46)
        LRBS.frame.origin = CGPoint(x: frame.width - 46, y: frame.height - 8)
        
        ULTS.backgroundColor = activeBlue
        ULBS.backgroundColor = activeBlue
        URTS.backgroundColor = activeBlue
        URBS.backgroundColor = activeBlue
        LLTS.backgroundColor = activeBlue
        LLBS.backgroundColor = activeBlue
        LRTS.backgroundColor = activeBlue
        LRBS.backgroundColor = activeBlue
        
        self.addSubview(ULTS)
        self.addSubview(ULBS)
        self.addSubview(URTS)
        self.addSubview(URBS)
        self.addSubview(LLTS)
        self.addSubview(LLBS)
        self.addSubview(LRTS)
        self.addSubview(LRBS)
        
        
        
        TBar = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 8))
        BBar = UIView(frame: CGRect(x: 0, y: frame.height - 8, width: frame.width, height: 8))
        LBar = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: frame.height))
        RBar = UIView(frame: CGRect(x: frame.width - 8, y: 0, width: 8, height: frame.height))
        
        TBar.backgroundColor = activeBlue
        LBar.backgroundColor = activeBlue
        RBar.backgroundColor = activeBlue
        BBar.backgroundColor = activeBlue

//        TBar.alpha = 0
//        BBar.alpha = 0
//        LBar.alpha = 0
//        RBar.alpha = 0
        
//        gr1.alpha = 0
//        gr2.alpha = 0
//        gr3.alpha = 0
//        gr4.alpha = 0
        
        gr1.frame = CGRect(x: (1/3) * frame.width, y: 0, width: 1.2, height: frame.height)
        gr2.frame = CGRect(x: (2/3) * frame.width, y: 0, width: 1.2, height: frame.height)
        gr3.frame = CGRect(x: 0, y: (1/3) * frame.height, width: frame.width, height: 1.2)
        gr4.frame = CGRect(x: 0, y: (2/3) * frame.height, width: frame.width, height: 1.2)
        
        gr1.backgroundColor = UIColor.white
        gr4.backgroundColor = UIColor.white
        gr3.backgroundColor = UIColor.white
        gr2.backgroundColor = UIColor.white
        
        self.addSubview(gr1)
        self.addSubview(gr2)
        self.addSubview(gr3)
        self.addSubview(gr4)
        self.addSubview(BBar)
        self.addSubview(TBar)
        self.addSubview(RBar)
        self.addSubview(LBar)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func cameraMode(){
                TBar.alpha = 0
                BBar.alpha = 0
                LBar.alpha = 0
                RBar.alpha = 0
        
                gr1.alpha = 0
                gr2.alpha = 0
                gr3.alpha = 0
                gr4.alpha = 0
    }
    
    func ViewEditMode(){
                TBar.alpha = 1
                BBar.alpha = 1
                LBar.alpha = 1
                RBar.alpha = 1
        
                gr1.alpha = 1
                gr2.alpha = 1
                gr3.alpha = 1
                gr4.alpha = 1
    }
    
    
}
