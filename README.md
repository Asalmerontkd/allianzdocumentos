## Instalación

* 1.- Copiar liga para clonar
* 3.- ionic plugin add pegar/link......

## Desinstalar

1.- ionic plugin remove io-mariachi-plugin-docs

## Método para documentos

allianzdocs.docFunctionLg('', funcionOk, funcionError);

## Método para identifiación

allianzdocs.docFunctionSm('', funcionOk, funcionError);

## Método para escanear tarjetas

allianzdocs.cardFunction('', funcionCard, funcionError);

## Funciones ejemplo


```
#!java Script

function funcionOk(response)
{
	//TODO aquí hacer lo que sea con el base 64, ejemplo:
	var datos = "data:image/jpg;base64," + response;
        document.getElementById('myImage').src = datos;
}

function funcionCard(response)
{
	//Obtener los datos de la tarjeta es un JSONobject
	var numTarjetaRedactado = response.redacted_card_number;
	var numTarjata = response.card_number;
	var mesExpira = response.expiry_month;
	var anioExpira = response.expiry_year;
	var tipo = response.card_type;

}

function funcionError(response)
{
	//TODO aquí muestra algún error como evento cancelado
}
```

## FUNCIONES ELIMINADAS

* allianzdocs.docFunctionLg
* allianzdocs.docFunctionSm